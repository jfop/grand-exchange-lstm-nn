import requests
import json
import bs4


# TODO: Make data go into a data directory
# TODO: Get future daily prices - run script everyday to get current day price
# TODO: Refactor


def main():
    # getItemData()
    # getItemPrices()
    updateAPIKey()


# Gets API key
# Returns: API key
def updateAPIKey():
    url = 'https://www.ge-tracker.com/osrs-market-watch'
    r = requests.get(url)
    for line in r.text.splitlines():
        if line.__contains__("<meta name=\"_token\" content="):
            APIkey = line
    soup = bs4.BeautifulSoup(APIkey, "lxml")
    link = soup.find("meta", {"name": "_token"})
    return str(link["content"])


def getItemPrices():
    # Needs to get past month data
    # https://www.ge-tracker.com/graph/10344/month
    try:
        print("Creating item price data file")
        data = getIDFile()

        # Gets List of ID's - sorted
        IDlist = []
        for k in data:
            IDlist.append(data[k]['item_id'])

        # Gets List of names - sorted
        name_list = []
        for k in data:
            name_list.append(data[k]['name'])

        data_dict = {}
        index_for_name_counter = 0
        # Loops through every item, gets graph info, stores it
        for x in IDlist:
            try:
                url = 'https://www.ge-tracker.com/graph/' + str(x) + '/month'
                payload = {}
                APIkey = updateAPIKey()
                headers = {'Authorization': 'Bearer '+ APIkey}
                response = requests.request('GET', url, headers=headers, data=payload, allow_redirects=False)
                tempDict = response.json()
                tempDict[name_list[index_for_name_counter]] = tempDict['data']
                del tempDict['data']
                data_dict.update(tempDict)
                print(tempDict)
                index_for_name_counter += 1
            except Exception:
                pass
        with open("item_prices.json", "w") as write_file:
            json.dump(data_dict, write_file, indent=4)

    except Exception as e:
        print('Exception found', format(e))


# Gets dictionary of all items and info about the items
# Returns:Full dictionary
def getItemData():
    try:
        print("Item data file found")
        with open('item_data.json') as json_file:
            data = json.load(json_file)
        return data
    except Exception as e:
        try:
            index_for_ID_counter = 0
            data_dict = {}

            print("Creating item data file")
            data = getIDFile()
            IDlist = []
            for k in data:
                IDlist.append(data[k]['item_id'])

            for x in IDlist:
                try:
                    url = 'https://www.ge-tracker.com/api/items/' + str(x)
                    payload = {}
                    APIkey = updateAPIKey()
                    headers = {
                        'Authorization': 'Bearer ' + APIkey}
                    response = requests.request('GET', url, headers=headers, data=payload, allow_redirects=False)
                    tempDict = response.json()
                    tempDict[index_for_ID_counter] = tempDict['data']
                    del tempDict['data']
                    data_dict.update(tempDict)
                    index_for_ID_counter += 1
                    print(response.text)

                except Exception:
                    pass
            with open("item_data.json", "w") as write_file:
                json.dump(data_dict, write_file, indent=4)

        except Exception as e:
            print('Exception found', format(e))


# Gets nested dictionary of all items including their id's
# Returns: Full dictionary of Names + ID
def getIDFile():
    try:
        print("ID file found")
        with open('item_ids.json') as json_file:
            data = json.load(json_file)
        return data
    except Exception as e:
        try:
            print("Creating ID file")
            url = 'https://www.ge-tracker.com/api/items/ids'
            payload = {}
            APIkey = updateAPIKey()
            headers = {'Authorization': 'Bearer ' + APIkey}
            response = requests.request('GET', url, headers=headers, data=payload, allow_redirects=False)
            with open("item_ids.json", "w") as write_file:
                json.dump(response.json(), write_file, indent=4)
        except Exception as e:
            print('Exception found', format(e))


if __name__ == '__main__':
    main()
